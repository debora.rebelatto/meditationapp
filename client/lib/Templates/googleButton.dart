import 'package:flutter/material.dart';

googleButton(context) {
  return Container(
    padding: EdgeInsets.only(top: 20),
    width: MediaQuery.of(context).size.width * 0.9,
    child: OutlinedButton(
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40),
        ),
        side: BorderSide(width: 1, color: Colors.grey),
      ),
      child: ListTile(
        leading: Image.asset('assets/social/Google.png', height: 30,),
        title: Text('CONTINUE WITH GOOGLE', textAlign: TextAlign.center, style: TextStyle(color: Colors.black ),),
      ),
      onPressed: (){},
    )
  );
}