import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meditation/Templates/Home.dart';

class ChooseTopic extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (context, index) => [
          SliverAppBar(
            //pinned: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.of(context).pop();
              },
              color: Colors.black,
            ),
            backgroundColor: Colors.transparent,
          )
        ],
        body: Container(
          padding: EdgeInsets.all(20),
          child: SingleChildScrollView(child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('What brings you', style: GoogleFonts.lato(fontSize: 30, fontWeight: FontWeight.bold)),
              Text('to this app?', style: GoogleFonts.lato(fontSize: 30, fontWeight: FontWeight.bold)),
              Text('Choose a topic to focus on', style: GoogleFonts.lato(fontSize: 20, fontWeight: FontWeight.w300)),
              SizedBox(height: 30),
              GridView.count(
                shrinkWrap: true,
                primary: false,
                padding: const EdgeInsets.all(10),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                children: <Widget>[
                  card(Color(0xff808AFF), 'Reduce Stress', Colors.white),
                  card(Color(0xffFA6E5A), 'Improve Performance', Colors.white),
                  card(Color(0xffFEB18F), 'Increase Happiness', Color(0xff3F4553)),
                  card(Color(0xffFFCF86), 'Reduce Anxiety', Color(0xff3F4553)),
                  card(Color(0xffFEB18F), 'Personal Growth', Colors.white),
                  card(Color(0xffFEB18F), 'Better Sleep', Colors.white),
                ],
              )
            ],
          ),
        )
      )),
      floatingActionButton: Center(child: Container(
        alignment: Alignment.bottomCenter,
        width: MediaQuery.of(context).size.width,
        child: OutlinedButton(child: Text('ok'), onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));
        }),
      )
    ));
  }


  card(Color color, String text, Color colorText) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color,
      ),
      padding: const EdgeInsets.all(15),
      child: Text(text, style: GoogleFonts.lato(color: colorText, fontWeight: FontWeight.bold, fontSize: 20)),
      );
  }
}