import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'ChooseTopic.dart';

class Welcome extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff8E97FD),
      body: Stack(children:[
        NestedScrollView(
          headerSliverBuilder: (context, index) => [ sliverAppBar(context) ],
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              padding: EdgeInsets.only(left: 25, right: 25),
              child: Stack(children: [
                Column(children: [
                  SizedBox(height: 20),
                  Text('Hi Afsar, Welcome ',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(textStyle: TextStyle(
                      fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xffFFECCC)
                    )),
                  ),
                  Text('to Silent Moon',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(textStyle: TextStyle(
                      fontSize: 30, fontWeight: FontWeight.w300, color: Color(0xffFFECCC)
                    )),
                  ),
                  SizedBox(height: 30),
                  Text('Explore the app, Find some peace of mind to prepare for meditation.',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.lato(textStyle: TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w300, color: Color(0xffEBEAEC)
                    )),
                  ),
                  SizedBox(height: 50),
                  Image.asset('assets/welcome.png'),
                  SizedBox(height: 50),
                  getStarted(context)
                ])
              ],)





            )
          )
        )
      ])
    );
  }

  sliverAppBar(context) {
    return SliverAppBar(
      pinned: false,
      elevation: 0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios, color: Colors.black),
        onPressed: () { Navigator.of(context).pop(); }
      ),
      backgroundColor: Colors.transparent,
      centerTitle: true,
      automaticallyImplyLeading: false,
      floating: true,
    );
  }

  getStarted(context) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      width: MediaQuery.of(context).size.width * 0.8,
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
        color: Color(0xffEBEAEC),
        child: Text('GET STARTED', style: GoogleFonts.lato(textStyle: TextStyle(
          color: Color(0xff3F414E)
        )),),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChooseTopic()));
        },
      )
    );
  }
}