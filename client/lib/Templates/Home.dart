import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String when;
  String name = 'Deb';
  initState() {
    super.initState();
    _setWhen();
  }

  _setWhen() {
    DateTime now = new DateTime.now();

    if(now.hour < 13) {
      setState(() { when = 'Morning'; });
    } else if(now.hour > 13) {
      setState(() { when = 'Evening'; });
    } else if(now.hour > 18) {
      setState(() { when = 'Night'; });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (context, index) => [ SliverAppBar(
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon:Icon(Icons.arrow_back_ios),
            color: Colors.black,
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        )],
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Good $when, $name!', style: GoogleFonts.lato(fontSize: 25, fontWeight: FontWeight.bold)),
              Text('Have a nice day!', style: GoogleFonts.lato(fontSize: 20)),
            ],
          )
        )
      )
    );
  }
}