import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meditation/Templates/Home.dart';

class Reminders extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        NestedScrollView(
          headerSliverBuilder: (context, index) => [ sliverAppBar(context) ],
          body: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('What time would you',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff3F414E)
                  )),
                ),

                Text('like to meditate?',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff3F414E)
                  )),
                ),

                SizedBox(height: 18),

                Text('Any time you can choose but We recommend',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xffA1A4B2)
                  )),
                ),

                Text('first thing in the morning.',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xffA1A4B2)
                  )),
                ),

                SizedBox(height: 20),

                Text('Which day would you',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff3F414E)
                  )),
                ),
                Text('like to meditate?',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff3F414E)
                  )),
                ),

                SizedBox(height: 18),

                Text('Everyday is best, but we recommend picking',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xffA1A4B2)
                  )),
                ),

                Text('at least five.',
                  style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.w300, color: Color(0xffA1A4B2)
                  )),
                ),

                Center(child: getStarted(context)),

                SizedBox(height: 18),

                Center(child: FlatButton(
                  child: Text('NO THANKS'),
                  onPressed: () {},
                ))

              ]
            )
          )
        ),
      ],),
    );
  }

  getStarted(context) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      width: MediaQuery.of(context).size.width * 0.8,
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
        color: Color(0xff8E97FD),
        child: Text('SAVE', style: GoogleFonts.lato(textStyle: TextStyle(
          color: Colors.white
        )),),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));
        },
      )
    );
  }

  sliverAppBar(context) {
    return SliverAppBar(
      pinned: false,
      elevation: 0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios, color: Colors.black),
        onPressed: () { Navigator.of(context).pop(); }
      ),
      backgroundColor: Colors.transparent,
      centerTitle: true,
      automaticallyImplyLeading: false,
      floating: true,
    );
  }

}