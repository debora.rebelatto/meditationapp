import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meditation/Templates/Welcome.dart';

import 'facebookButton.dart';
import 'googleButton.dart';

class Signup extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children:[
        NestedScrollView(
          headerSliverBuilder: (context, index) => [ sliverAppBar(context) ],
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              padding: EdgeInsets.only(left: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Create your account', style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 30, fontWeight: FontWeight.bold, color: Color(0xff3F414E)
                  )),),
                  facebookButton(context), googleButton(context),
                  SizedBox(height: 20,),
                  Text('OR LOG IN WITH EMAIL', style: GoogleFonts.lato(textStyle: TextStyle(
                    fontSize: 15, fontWeight: FontWeight.w400, color: Color(0xffA1A4B2)
                  )),),
                  SizedBox(height: 20,),
                  fields(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('I have read the Privacy Policy', style: GoogleFonts.lato(textStyle: TextStyle(
                        fontSize: 15, fontWeight: FontWeight.w400, color: Color(0xffA1A4B2)
                      )),),
                      Text("checkbox")
                    ],
                  ),
                  getStarted(context),
                ],
              )
            )
          )
        )
      ])
    );
  }

  sliverAppBar(context) {
    return SliverAppBar(
      pinned: false,
      elevation: 0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios, color: Colors.black),
        onPressed: () { Navigator.of(context).pop(); }
      ),
      expandedHeight: 100,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      automaticallyImplyLeading: false,
      floating: true,
    );
  }

  fields() {
    return Column(children: [
      Padding(
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: TextFormField(
          decoration: decoration('Name'),
          validator: (value) => value.isEmpty ? 'Field is empty' : '',
        )
      ),
      Padding(
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: TextFormField(
          decoration: decoration('Email'),
          validator: (value) => value.isEmpty ? 'Field is empty' : '',
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 5, bottom: 20),
        child: TextFormField(
          obscureText: true,
          decoration: decoration('Password'),
          validator: (value) => value.isEmpty ? 'Field is empty' : '',
        )
      )
    ],);
  }

  decoration(hint) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0),
        borderSide: BorderSide(color: Colors.white, width: 0.0),
      ),
      focusedBorder:OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.white, width: 2.0),
        borderRadius: BorderRadius.circular(20.0),
      ),
      border: new OutlineInputBorder( borderRadius: new BorderRadius.circular(20.0)),
      contentPadding: EdgeInsets.fromLTRB(30, 20, 10, 20),
      hintText: hint,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      filled: true,
      fillColor: Color(0xffF2F3F7)
    );
  }

  getStarted(context) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      width: MediaQuery.of(context).size.width * 0.8,
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(40) ),
        color: Color(0xff8E97FD),
        child: Text('GET STARTED', style: GoogleFonts.lato(textStyle: TextStyle(
          color: Colors.white
        )),),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => Welcome()));
        },
      )
    );
  }

  forgotPassword() {
    return Container(
      child: FlatButton(
        child: Text('Forgot Password?'),
        onPressed: (){},
      )
    );
  }
}