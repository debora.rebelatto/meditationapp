import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meditation/Templates/SignIn.dart';
import 'package:meditation/Templates/Signup.dart';

class SigninSignup extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(40),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/signinup.png'),

              SizedBox(height: 50),

              Text('We are what we do',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff3F414E)
                )
              ),

              Text('Thousand of people are usign silent moon for smalls meditation',
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                  color: Color(0xffA1A4B2),
                )
              ),

              SizedBox(height: 50),

              Column(children: [
                signUpButton(context),
                signInButton(context)
              ],)
            ],
          ),
        )




      )
    );
  }

  signUpButton(context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      width: MediaQuery.of(context).size.width * 0.9,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          backgroundColor: Color(0xff8E97FD),
          padding: EdgeInsets.all(20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          side: BorderSide(width: 1, color: Colors.grey),
        ),
        child: Text('SIGN UP', style: GoogleFonts.lato(textStyle: TextStyle(color: Colors.white )),),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => Signup()));
        },
      )
    );
  }

  signInButton(context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: FlatButton(
        child: Text('ALREADY HAVE AN ACCOUNT? LOG IN',
          style: GoogleFonts.lato(textStyle: TextStyle(color: Color(0xffA1A4B2) ))
        ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignIn()));
          },
      )
    );
  }
}