#!/bin/bash

GREEN=`tput setaf 2`
reset=`tput sgr0`

PS3='Please enter your choice: '
options=(
  "Build Apk"
  "Splash Screen"
  "Clean"
  "Quit"
)

select opt in "${options[@]}"
do
  case $opt in
    "Build Apk")
      echo "${GREEN} More info: https://flutter.dev/docs/deployment/android ${reset}"
      echo "${GREEN} Release Apk can be found at ~/build/app/outputs/flutter-apk ${reset}"
      flutter build apk --release
      echo "${GREEN}Done!"
      break
      ;;
    "Splash Screen")
      echo "${GREEN}More https://pub.dev/packages/flutter_native_splash ${reset}"
      flutter pub pub run flutter_native_splash:create
      break
      ;;
    "Clean")
      flutter clean
      break
      ;;
    "Quit")
      break
      ;;
    *) echo "invalid option $REPLY";;
  esac
done