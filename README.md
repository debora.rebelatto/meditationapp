# :last_quarter_moon_with_face: Meditation app

Based in Silent Moon app design [Figma](https://www.figma.com/community/file/882888114457713282)

Built for learning purposes using Flutter.

<p align="center">
<img src="./video.gif" width="300" height="500"/>
</p>
